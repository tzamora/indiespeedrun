﻿using UnityEngine;
using System.Collections;

public enum MonkeyTypeEnum
{
	ExplosiveMonkey,
	TimeMonkey,
	MegaJumpMonkey
}

public class MonkeySpawnerMediator : MonoBehaviour {
	
	public MonkeyTypeEnum MonkeyType = MonkeyTypeEnum.ExplosiveMonkey;
	
	public GameObject ExplosionMonkeyPrefab;
	
	public GameObject MegaJumpMonkeyPrefab;
	
	public GameObject TimeMonkeyPrefab;
	
	// Use this for initialization
	void Start () 
	{
		InvokeRepeating("SpawnMonkey", 5f, 5f);
	}
	
	void SpawnMonkey()
	{
		switch(MonkeyType)
		{
			case MonkeyTypeEnum.ExplosiveMonkey:
				Spawner.Spawn(ExplosionMonkeyPrefab, transform.position + new Vector3(0f, 1f, 0f), Quaternion.identity);
			break;
			case MonkeyTypeEnum.TimeMonkey:
				Spawner.Spawn(TimeMonkeyPrefab, transform.position + new Vector3(0f, 1f, 0f), Quaternion.identity);
			break;
			case MonkeyTypeEnum.MegaJumpMonkey:
				Spawner.Spawn(MegaJumpMonkeyPrefab, transform.position + new Vector3(0f, 1f, 0f), Quaternion.identity);
			break;
		}
	}
}
